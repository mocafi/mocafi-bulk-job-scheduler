const express = require('express');
const cors = require('cors');
const schedulerRouter = require('./routes');
const ircRouter = require('./routes/irc');
const fileUpload = require('express-fileupload');
const adminSchedulerRouter = require('./routes/adminSchedulerRouter');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

app.get('/v4/scheduler/_healthcheck', (req, res) => {
  res.status(200).send('OK');
});

app.use('/v4/scheduler/admin-portal', schedulerRouter);
app.use('/v4/scheduler/irc-portal', ircRouter);

app.use('/oneapp/scheduler/admin', adminSchedulerRouter);

module.exports = app;
