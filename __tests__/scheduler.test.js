jest.mock('../services');
jest.mock('../services/irc');
jest.mock('../services/sqs');
jest.mock('../irc/processor');
const scheduler = require('../scheduler');
const services = require('../services');
const ircServices = require('../services/irc');
const CronExpression = require('../utils/cron-expressions');
const irc = require('../irc/processor');
const { jobStatusEnum, jobTypeEnum } = require('../utils/enums');

describe('scheduler.js tests', () => {
  const fakeTask = {
    _id: '619bb01e8700c8001257ab9c',
    prodId: '2088',
    programName: 'mocafi-irc',
    process: 'load-funds',
    scheduledBy: 'tester@teravisiontech.com',
    sourceLocation: 'https://fake.s3.us-east-1.amazonaws.com/Payment_Bulk.csv',
    timesProcessed: [],
    fileName: 'Payment_Bulk.csv',
    cronSyntax: CronExpression.EVERY_SECOND,
    type: jobTypeEnum.recurrent,
    status: jobStatusEnum.active,
  };

  const timezone = 'America/Los_Angeles';

  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    scheduler.destroyAllTasks();
    jest.clearAllTimers();
    jest.clearAllMocks();
  });

  describe('Task add/delete tests', () => {
    it('should create a new scheduled task', () => {
      const mockGetTaskById = jest
        .spyOn(services, 'getTaskById')
        .mockImplementation(() => fakeTask);
      scheduler.newTask(fakeTask, { timezone });
      jest.advanceTimersByTime(3000);
      expect(mockGetTaskById.mock.calls.length).toBe(3);
    });

    it('should delete an scheduled task', () => {
      scheduler.newTask(fakeTask, { timezone });
      const deleted = scheduler.destroyTask(fakeTask._id);
      expect(deleted).toBe(true);
    });

    it('should NOT delete an scheduled task - task has not been created', () => {
      const deleted = scheduler.destroyTask(fakeTask._id);
      expect(deleted).toBe(false);
    });

    it('should create a new IRC scheduled task', () => {
      const mockGetIrcTask = jest
        .spyOn(ircServices, 'getIrcTaskById')
        .mockImplementation(() => null);
      scheduler.newTask(fakeTask, { timezone, isIRC: true, dbName: '2088' });
      jest.advanceTimersByTime(1000);
      expect(mockGetIrcTask).toHaveBeenCalled();
    });
  });

  describe('Triggers tests', () => {
    describe('taskTriggerIRC', () => {
      const mockProcessTask = jest
        .spyOn(irc, 'processTask')
        .mockImplementation(() => {});

      it('should trigger IRC with correct params', async () => {
        const mockGetIrcTask = jest
          .spyOn(ircServices, 'getIrcTaskById')
          .mockImplementation(() => {
            return fakeTask;
          });

        await scheduler.taskTriggerIRC(fakeTask._id, '2088');

        expect(mockGetIrcTask).toHaveBeenCalled();
        expect(mockGetIrcTask.mock.calls[0][0]).toBe(fakeTask._id);
        expect(mockGetIrcTask.mock.calls[0][1]).toEqual({
          dbName: '2088',
        });
        expect(mockProcessTask).toHaveBeenCalled();
        expect(mockProcessTask.mock.calls[0][0]).toEqual(fakeTask);
        expect(mockProcessTask.mock.calls[0][1]).toBe('2088');
      });

      it('should change the task status to complete - end date is past', async () => {
        const mockGetIrcTask = jest
          .spyOn(ircServices, 'getIrcTaskById')
          .mockImplementation(() => {
            const yesterday = new Date(Date.now() - 86400000);
            return { ...fakeTask, endsAt: yesterday };
          });

        const mockUpdateIrcTask = jest
          .spyOn(ircServices, 'updateIrcTask')
          .mockImplementation(() => {});

        await scheduler.taskTriggerIRC(fakeTask._id, '2088');

        expect(mockGetIrcTask).toHaveBeenCalled();
        expect(mockProcessTask.mock.calls.length).toBe(0);
        expect(mockUpdateIrcTask).toHaveBeenCalled();
        expect(mockUpdateIrcTask.mock.calls[0][0]).toEqual({
          _id: fakeTask._id,
        });
        expect(mockUpdateIrcTask.mock.calls[0][1]).toEqual({
          status: jobStatusEnum.completed,
        });
        expect(mockUpdateIrcTask.mock.calls[0][2]).toEqual({
          dbName: '2088',
          customOperators: {
            $push: {
              history: {
                action: jobStatusEnum.completed,
                date: new Date(),
              },
            },
          },
        });
      });

      it('should change task status to complete - task is type unique', async () => {
        const mockGetIrcTask = jest
          .spyOn(ircServices, 'getIrcTaskById')
          .mockImplementation(() => {
            return { ...fakeTask, type: jobTypeEnum.unique };
          });

        const mockUpdateIrcTask = jest
          .spyOn(ircServices, 'updateIrcTask')
          .mockImplementation(() => {});

        await scheduler.taskTriggerIRC(fakeTask._id, '2088');

        expect(mockGetIrcTask).toHaveBeenCalled();
        expect(mockProcessTask.mock.calls.length).toBe(1);
        expect(mockUpdateIrcTask).toHaveBeenCalled();
        expect(mockUpdateIrcTask.mock.calls[0][0]).toEqual({
          _id: fakeTask._id,
        });
        expect(mockUpdateIrcTask.mock.calls[0][1]).toEqual({
          status: jobStatusEnum.completed,
        });
        expect(mockUpdateIrcTask.mock.calls[0][2]).toEqual({
          dbName: '2088',
          customOperators: {
            $push: {
              history: {
                action: jobStatusEnum.completed,
                date: new Date(),
              },
            },
          },
        });
      });

      it('should NOT change task status to complete - task is type recurrent', async () => {
        const mockGetIrcTask = jest
          .spyOn(ircServices, 'getIrcTaskById')
          .mockImplementation(() => {
            return fakeTask;
          });

        const mockUpdateIrcTask = jest
          .spyOn(ircServices, 'updateIrcTask')
          .mockImplementation(() => {});

        await scheduler.taskTriggerIRC(fakeTask._id, '2088');

        expect(mockGetIrcTask).toHaveBeenCalled();
        expect(mockProcessTask.mock.calls.length).toBe(1);
        expect(mockUpdateIrcTask.mock.calls.length).toBe(0);
      });

      it('should NOT call irc.processTask - task not found in DB', async () => {
        const mockGetIrcTask = jest
          .spyOn(ircServices, 'getIrcTaskById')
          .mockImplementation(() => null);
        const mockUpdateIrcTask = jest
          .spyOn(ircServices, 'updateIrcTask')
          .mockImplementation(() => {});

        await scheduler.taskTriggerIRC(fakeTask._id, '2088');

        expect(mockGetIrcTask).toHaveBeenCalled();
        expect(mockProcessTask.mock.calls.length).toBe(0);
        expect(mockUpdateIrcTask.mock.calls.length).toBe(0);
      });

      it('should NOT call irc.processTask - task status is not active', async () => {
        const taskStatusNotActive = Object.values(jobStatusEnum).filter(
          (status) => status !== jobStatusEnum.active
        );

        for (const status of taskStatusNotActive) {
          const mockGetIrcTask = jest
            .spyOn(ircServices, 'getIrcTaskById')
            .mockImplementation(() => ({ ...fakeTask, status }));
          const mockUpdateIrcTask = jest
            .spyOn(ircServices, 'updateIrcTask')
            .mockImplementation(() => {});

          await scheduler.taskTriggerIRC(fakeTask._id, '2088');

          expect(mockGetIrcTask).toHaveBeenCalled();
          expect(mockProcessTask.mock.calls.length).toBe(0);
          expect(mockUpdateIrcTask.mock.calls.length).toBe(0);
        }
      });
    });
  });
});
