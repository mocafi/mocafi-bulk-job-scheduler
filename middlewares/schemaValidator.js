const Joi = require('joi');

function schemaValidator(schema) {
  return function validate(req, res, next) {
    const { params, query, body } = req;

    if (!schema || !Joi.isSchema(schema)) {
      return next();
    }

    const { error, value } = schema.validate(
      { params, query, body },
      { stripUnknown: false, allowUnknown: true, abortEarly: false }
    );

    if (error) {
      return res
        .status(400)
        .send({ Success: 0, Error: 1, Data: error.message });
    }

    req.body = value.body;
    req.params = value.params;
    req.query = value.query;
    return next();
  };
}

module.exports = {
  schemaValidator,
};
