/*
const multer = require('multer');
const multerS3 = require('multer-s3');
*/
const { bucket, uploadObject } = require('../services/aws-s3');
const { uploadFileTypes } = require('../utils/enums');
const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
/*
const csvTaskUploader = multer({
  storage: multerS3({
    s3: awsS3,
    bucket:
      process.env.NODE_ENV === 'production'
        ? `mocafi-galileo-bulk-upload-prod${
            process.env.AWS_REGION === 'us-west-1' ? '-west' : ''
          }`
        : 'mocafi-galileo-bulk-upload',
    acl: 'public-read',
    contentType: multerS3.AUTO_CONTENT_TYPE,
    key: (req, file, cb) => {
      const programName = req.query.programName || '';
      const process = req.query.process;
      const originalName = file.originalname;
      const name = `${process}_${programName}_${new Date().getTime()}_${originalName}`;
      req.query.fileName = name;
      cb(null, name);
    },
  }),
});
*/

const csvTaskUploaderToS3 = async (req, res = response, next) => {
  if (!req.files) {
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'A CSV file is required to create the scheduled job',
    });
  }

  if (
    Object.keys(req.files).length === 0 ||
    Object.keys(req.files).length > 1
  ) {
    logger.error(
      'File object in the request is empty, or more than 1 file has been uploaded'
    );
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'File object in the request is empty, or more than 1 file has been uploaded',
    });
  }
  const entries = Object.entries(req.files);
  for (const [key, file] of entries) {
    if (!Object.values(uploadFileTypes).includes(file.mimetype)) {
      logger.error('Invalid file type');
      return res.status(400).json({
        Success: 0,
        Error: 1,
        Data: 'Invalid file typr',
      });
    }
    req.file = file;
  }

  const programName = req.query.programName || '';
  const process = req.query.process;
  const originalName = req.file.name;
  const name = `${process}_${programName}_${new Date().getTime()}_${originalName}`;
  req.query.fileName = name;

  const s3Response = await uploadObject(bucket, name, req.file.data);

  if (!s3Response || !s3Response.ETag) {
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'error while uploading submitted file to S3',
    });
  }

  req.file.location = `https://${bucket}.s3.us-east-1.amazonaws.com/${name}`;
  logger.info(`file ${req.file.name} successfully uploaded to S3 as ${name}`);

  next();
};

const checkUserPermissions = (permissions = []) => {
  return (req, res, next) => {
    try {
      const reqUserPermissions = req.user['custom:scope'].split(';');

      const hasPermission = reqUserPermissions.some((reqUserPerm) =>
        permissions.includes(reqUserPerm)
      );

      if (!hasPermission) {
        return res.status(401).send({
          Success: 0,
          Error: 1,
          Data: 'user unauthorized to access requested resource',
        });
      }
      next();
    } catch (error) {
      logger.error(error.message);
      return res.status(401).send({
        Success: 0,
        Error: 1,
        Data: 'user unauthorized to access requested resource',
      });
    }
  };
};

//module.exports = { csvTaskUploader };
module.exports = { csvTaskUploaderToS3, checkUserPermissions };
