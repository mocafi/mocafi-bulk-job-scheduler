const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const cron = require('node-cron');
const ircService = require('../services/irc');
const scheduler = require('../scheduler');
const { jobStatusEnum } = require('../utils/enums');

const createIrcTask = async (req, res) => {
  try {
    const { taskId, dbName } = req.body;

    const task = await ircService.getIrcTaskById(taskId, { dbName });

    if (!task) {
      throw new Error(`task ${taskId} not found in ${dbName}`);
    }

    if (
      ![
        jobStatusEnum.created,
        jobStatusEnum.pendingForReview,
        jobStatusEnum.paused,
      ].includes(task.status)
    ) {
      throw new Error(`task ${taskId} has an invalid status '${task.status}'`);
    }

    if (!cron.validate(task.cronSyntax)) {
      throw new Error(
        `task ${taskId} has an invalid cron syntax ${task.cronSyntax}`
      );
    }

    scheduler.newTask(task, { timezone: task.timezone, isIRC: true, dbName });

    return res.status(200).send({
      Success: 1,
      Error: 0,
      Data: task._id,
    });
  } catch (error) {
    logger.error(`POST /v4/scheduler/irc-portal error - ${error.message}`);
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: error.message,
    });
  }
};

const deleteIrcTask = async (req, res) => {
  try {
    const { taskId } = req.params;

    const deleted = scheduler.destroyTask(taskId);

    if (!deleted) {
      throw new Error(`task ${taskId} could not be deleted`);
    }

    return res.status(200).send({
      Success: 1,
      Error: 0,
      Data: taskId,
    });
  } catch (error) {
    logger.error(
      `DELETE /v4/scheduler/irc-portal/${req.params.taskId} error - ${error.message}`
    );
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: error.message,
    });
  }
};

module.exports = {
  createIrcTask,
  deleteIrcTask,
};
