const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const slack = require('mocafi-support-libraries').Slack;
const { ObjectId } = require('mongodb');
const {
  insertTaskToDB,
  updateTaskById,
  getTasksPagination,
  getTaskById,
} = require('../services');
const scheduler = require('../scheduler');
const { customCronsyntax } = require('../helpers');
const CronExpression = require('../utils/cron-expressions');

const channel =
  process.env.NODE_ENV === 'production' ? '#admin-logs' : '#dev-logs';

const getTaskByIdController = async (req, res) => {
  try {
    const { id } = req.params;

    const task = await getTaskById(new ObjectId(id));

    return res.status(200).send({
      Success: 1,
      Error: 0,
      Data: task,
    });
  } catch (error) {
    logger.error('error getting task by id');
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error getting task by id',
    });
  }
};

const getTaskController = async (req, res) => {
  try {
    const {
      query: { limit = 10, page = 1, sort = -1, sortName = 'createdAt' },
    } = req;

    const tasks = await getTasksPagination(
      {},
      {
        limit: parseInt(limit),
        page: parseInt(page),
        sort: parseInt(sort),
        sortName,
      }
    );

    return res.status(200).send({
      Success: 1,
      Error: 0,
      Data: tasks,
    });
  } catch (error) {
    console.log(error);
    logger.error('error getting tasks');
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error getting tasks',
    });
  }
};

const getTaskControllerOneapp = async (req, res) => {
  try {
    const {
      query: { limit = 10, page = 1, sort = -1, sortName = 'createdAt' },
    } = req;

    const tasks = await getTasksPagination(
      {},
      {
        limit: parseInt(limit),
        page: parseInt(page),
        sort: parseInt(sort),
        sortName,
      }
    );
    tasks.currentPage = parseInt(page);
    tasks.pageItems = tasks.tasks.length;
    return res.status(200).send({
      Success: 1,
      Error: 0,
      Data: {
        result: tasks.tasks,
        totalItems: tasks.totalItems,
        totalPages: tasks.totalPages,
        currentPage: parseInt(page),
        pageItems: tasks.tasks.length,
      },
    });
  } catch (error) {
    console.log(error);
    logger.error('error getting tasks');
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error getting tasks',
    });
  }
};

const createTaskController = async (req, res) => {
  try {
    const timezone = /angeleno/i.test(req.query.programName)
      ? 'America/Los_Angeles'
      : 'America/New_York';

    const cronSyntax =
      CronExpression[req.query.cronExpression] ||
      customCronsyntax(req.query.customDate);

    const taskInserted = await insertTaskToDB({
      ...req.query,
      timezone,
      cronSyntax,
      sourceLocation: req.file.location,
      scheduledBy: req.headers['mocafi-username'],
    });

    scheduler.newTask(taskInserted, { timezone });

    slack.sendSlackMessage({
      username: `TASKS SCHEDULER`,
      icon_emoji: ':timer_clock:',
      text: `A new task '${req.query.name}' has been created by ${req.headers['mocafi-username']}`,
      channel,
    });
    logger.info(
      `A new task '${req.query.name}' has been created by ${req.headers['mocafi-username']}`
    );
    return res.status(200).json({ Success: 1, Error: 0, Data: taskInserted });
  } catch (error) {
    logger.error(`error creating new task ${JSON.stringify(req.query)}`);
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error scheduling task',
    });
  }
};

const editTaskController = async (req, res) => {
  try {
    const fields = ['name', 'description'];

    let updateObject = {};

    fields.forEach((field) => {
      if (req.body[field]) updateObject[field] = req.body[field];
    });

    await updateTaskById(req.params.id, updateObject);

    return res.json({
      Success: 1,
      Error: 0,
      Data: `Task ${req.params.id} has been edited successfully`,
    });
  } catch (error) {
    logger.error(`error editing task Id=${req.params.id}`);
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error editing task',
    });
  }
};

const deleteTaskController = async (req, res) => {
  try {
    const { id } = req.params;

    const deleted = scheduler.destroyTask(id);

    if (!deleted) {
      return res.status(400).json({
        Success: 0,
        Error: 1,
        Data: "Couldn't delete the task",
      });
    }

    await updateTaskById(id, {
      status: 'destroyed',
      destroyedBy: req.headers['mocafi-username'],
      destroyedAt: new Date(),
      destroyed: true,
    });

    return res
      .status(200)
      .json({ Success: 1, Error: 0, Data: `task ${id} deleted` });
  } catch (error) {
    logger.error(`error deleting task Id=${req.params.id}`);
    return res.status(400).json({
      Success: 0,
      Error: 1,
      Data: 'Error deleting task',
    });
  }
};

const getCronExpressionsController = (req, res) => {
  const cronExpressions = Object.getOwnPropertyNames(CronExpression);
  return res.status(200).json({ Success: 1, Error: 0, Data: cronExpressions });
};

module.exports = {
  createTaskController,
  deleteTaskController,
  getCronExpressionsController,
  getTaskController,
  getTaskControllerOneapp,
  getTaskByIdController,
  editTaskController,
};
