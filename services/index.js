const db = require('mocafi-support-libraries').DB;
const { ObjectId } = require('mongodb');

const getTaskById = (_id) => {
  const tasksCollection = db.getDB().collection('tasks-scheduler');
  return tasksCollection.findOne({ _id });
};

const getTasksPagination = async (
  query = {},
  { limit, page, sort, sortName }
) => {
  const tasksCollection = db.getDB().collection('tasks-scheduler');

  const tasks = await tasksCollection
    .find(query)
    .skip(limit * page - limit)
    .limit(limit)
    .sort({ [sortName]: sort })
    .toArray();

  const totalTasks = await tasksCollection.countDocuments(query);

  return {
    tasks,
    totalItems: totalTasks,
    totalPages: Math.ceil(totalTasks / limit),
  };
};

const getTasks = (query = {}) => {
  const tasksCollection = db.getDB().collection('tasks-scheduler');
  return tasksCollection.find(query);
};

const insertTaskToDB = async (data) => {
  const tasksCollection = db.getDB().collection('tasks-scheduler');

  const taskInserted = await tasksCollection.insertOne({
    ...data,
    createdAt: new Date(),
    status: 'active',
    destroyed: false,
    timesProcessed: [],
  });

  return taskInserted.ops[0];
};

const updateTaskById = (_id, newObject, custom = {}) => {
  const tasksCollection = db.getDB().collection('tasks-scheduler');

  const newValues = {
    $set: {
      ...newObject,
    },
    ...custom,
  };

  return tasksCollection.updateOne({ _id: new ObjectId(_id) }, newValues);
};

module.exports = {
  getTaskById,
  insertTaskToDB,
  updateTaskById,
  getTasks,
  getTasksPagination,
};
