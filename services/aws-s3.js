const { awssdk } = require('mocafi-support-libraries').AWS;

const bucket =
  process.env.NODE_ENV === 'production'
    ? `mocafi-galileo-bulk-upload-prod${
        process.env.AWS_REGION === 'us-west-1' ? '-west' : ''
      }`
    : 'mocafi-galileo-bulk-upload';

const s3 = new awssdk.S3({
  version: 'latest',
  region: process.env.AWS_REGION,
  endpoint: `https://s3.${process.env.AWS_REGION}.amazonaws.com`,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const uploadObject = async (bucket, key, fileData) => {
  const uploadParams = {
    Bucket: bucket,
    Key: key,
    Body: fileData,
    ACL: 'public-read',
  };

  const uploadResult = await s3.putObject(uploadParams).promise();
  return uploadResult;
};

module.exports = {
  awsS3: s3,
  bucket,
  uploadObject,
};
