const { awssdk } = require('mocafi-support-libraries').AWS;
const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const { v4: uuidv4 } = require('uuid');

const sqs = new awssdk.SQS({
  version: 'latest',
  region: process.env.AWS_REGION,
  endpoint: `https://s3.${process.env.AWS_REGION}.amazonaws.com`,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

const AWS_ACCOUNT_ID = process.env.AWS_ACCOUNT_ID;

const queueURL =
  process.env.NODE_ENV == 'development'
    ? `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${AWS_ACCOUNT_ID}/DevGalileoBulk.fifo`
    : `https://sqs.${process.env.AWS_REGION}.amazonaws.com/${AWS_ACCOUNT_ID}/GalileoBulk.fifo`;

function writeSQS(message, options = {}) {
  const { MessageGroupId = uuidv4(), MessageDeduplicationId = uuidv4() } =
    options;

  logger.info(
    `Writing SQS Message: ${message}, MessageDeduplicationId=${MessageDeduplicationId}`
  );
  let params = {
    MessageBody: `${message}`,
    QueueUrl: queueURL,
    MessageGroupId,
    MessageDeduplicationId,
  };

  sqs.sendMessage(params, (error, data) => {
    if (error) {
      logger.error(`writeSQS() Error - ${error.message}`);
    } else {
      logger.info(`writeSQS() Message sent: ${JSON.stringify(data)}`);
    }
  });
}

module.exports = {
  writeSQS,
};
