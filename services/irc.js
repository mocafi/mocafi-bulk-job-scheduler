const db = require('mocafi-support-libraries').DB;
const { ObjectId } = require('mongodb');
const { jobStatusEnum } = require('../utils/enums');

const getIrcProdIds = async () => {
  const municipalitiesCollection = db
    .getDB('MoCaFi-IRC')
    .collection('municipalities');

  const prodIds = await municipalitiesCollection
    .aggregate([{ $group: { _id: '$prodId' } }])
    .toArray();

  return prodIds.map(({ _id }) => _id);
};

const getOrdersToProcess = async () => {
  const prodIds = await getIrcProdIds();

  const findOrdersQuery = prodIds.map((prodId) =>
    db
      .getDB(prodId)
      .collection('orders')
      .find({ status: 'pending for processing' })
      .toArray()
  );

  const ordersToProcess = (await Promise.all(findOrdersQuery)).flat();

  return ordersToProcess;
};

const getIrcTaskById = (taskId, { dbName }) => {
  const tasksCollection = db.getDB(dbName).collection('tasks-scheduler');
  return tasksCollection.findOne({ _id: ObjectId(taskId) });
};

const getTasksToPurge = async () => {
  const prodIds = await getIrcProdIds();

  const findTaksToPurge = prodIds.map((prodId) =>
    db
      .getDB(prodId)
      .collection('tasks-scheduler')
      .aggregate([
        {
          $match: { endsAt: { $lt: new Date() }, status: jobStatusEnum.active },
        },
        {
          $addFields: {
            prodId,
          },
        },
      ])
      .toArray()
  );

  const tasks = (await Promise.all(findTaksToPurge)).flat();

  return tasks;
};

const updateIrcTask = (
  query = {},
  newObject,
  { dbName, customOperators = {} }
) => {
  const tasksCollection = db.getDB(dbName).collection('tasks-scheduler');

  const newValues = {
    $set: {
      ...newObject,
    },
    ...customOperators,
  };

  return tasksCollection.updateOne(query, newValues);
};

const getIrcTasks = (query = {}, { dbName }) => {
  const tasksCollection = db.getDB(dbName).collection('tasks-scheduler');
  return tasksCollection.find(query).toArray();
};

const updateOneIrcCard = (query = {}, newProperties, { dbName }) => {
  const cardsCollection = db.getDB(dbName).collection('cards');
  const newValues = {
    $set: {
      ...newProperties,
    },
  };

  return cardsCollection.updateOne(query, newValues);
};

const updateManyIrcCard = (query, newProperties, { dbName }) => {
  const cardsCollection = db.getDB(dbName).collection('cards');

  const newValues = {
    $set: {
      ...newProperties,
    },
  };

  return cardsCollection.updateMany(query, newValues);
};

const updateFundingRequest = (query, newProperties, { dbName }) => {
  const fundingRequestsCollection = db
    .getDB(dbName)
    .collection('funding-requests');

  const newValues = {
    $set: {
      ...newProperties,
    },
  };

  return fundingRequestsCollection.updateMany(query, newValues);
};

const getFundingRequestsToProcess = async () => {
  const prodIds = await getIrcProdIds();

  const findFundingRequestsToProcess = prodIds.map((prodId) =>
    db
      .getDB(prodId)
      .collection('funding-requests')
      .aggregate([
        {
          $match: {
            status: 'pending',
            createdAt: { $lt: new Date(Date.now() - 15 * 60 * 1000) },
            automaticApproval: true,
          },
        },
        {
          $addFields: {
            prodId,
          },
        },
      ])
      .toArray()
  );

  const fundingRequests = (
    await Promise.all(findFundingRequestsToProcess)
  ).flat();

  return fundingRequests;
};

module.exports = {
  getOrdersToProcess,
  getIrcTaskById,
  getIrcProdIds,
  getIrcTasks,
  updateIrcTask,
  updateOneIrcCard,
  updateManyIrcCard,
  getTasksToPurge,
  updateFundingRequest,
  getFundingRequestsToProcess,
};
