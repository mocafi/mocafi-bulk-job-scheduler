const db = require('mocafi-support-libraries').DB;

const getPassManualKycReviews = async () => {
  const manualKycReviewCollection = db.getDB().collection('manual-kyc-review');
  return manualKycReviewCollection.find({ status: 'PASS' }).toArray();
};

module.exports = { getPassManualKycReviews };
