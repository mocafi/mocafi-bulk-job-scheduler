(async () => {
  require('dotenv').config();
  await require('mocafi-support-libraries').initialize();
  const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
  const { runTasks } = require('./scheduler');
  const db = require('mocafi-support-libraries').DB;
  const app = require('./app');
  const { schedulerBackup } = require('./helpers');

  const port = process.env.PORT || 7021;

  db.connect((err) => {
    if (err) {
      logger.error(`DB Server Connection Error ${err}`);
      process.exit(1);
    }
    logger.info('db connected');
    schedulerBackup();
    runTasks();
  });

  app.listen(port, () => {
    logger.info(`Server listening on port ${port}`);
  });
})();
