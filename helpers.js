const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const scheduler = require('./scheduler');
const { getTasks } = require('./services');
const { getIrcProdIds, getIrcTasks } = require('./services/irc');
const { jobStatusEnum } = require('./utils/enums');

const backupTasks = async () => {
  try {
    const tasks = await getTasks({ status: 'active', destroyed: false });

    logger.info('Backup has started');

    tasks.forEach((task) => {
      logger.info(`Rescheduling for task: ${JSON.stringify(task)}`);
      scheduler.newTask(task, { timezone: task.timezone });
    });

    logger.info('Backup has ended');
  } catch (error) {
    logger.error(`Error doing backup scheduler`);
  }
};

const backupIrcTask = async (dbName) => {
  try {
    logger.info(`backupIrcTask(${dbName}) start`);

    const tasks = await getIrcTasks(
      { status: jobStatusEnum.active },
      { dbName }
    );

    if (tasks.length > 0) {
      for (const task of tasks) {
        logger.info(`backupIrcTask(${dbName}) rescheduling - task ${task._id}`);
        scheduler.newTask(task, {
          timezone: task.timezone,
          isIRC: true,
          dbName,
        });
      }
    }

    logger.info(`backupIrcTask(${dbName}) finished`);
  } catch (error) {
    logger.error(`backupIrcTask(${dbName}) error - ${error.message}`);
  }
};

const backupIrcTasks = async () => {
  try {
    logger.info('backupIrcTasks() start');
    const prodIds = await getIrcProdIds();
    for (const prodId of prodIds) {
      await backupIrcTask(prodId);
    }
  } catch (error) {
    logger.error(`backupIrcTasks() error - ${error.message}`);
  }
};

const schedulerBackup = () => {
  backupTasks();
  backupIrcTasks();
};

const customCronsyntax = (date) => {
  const customDate = new Date(date);
  const cronSyntax = `${customDate.getMinutes()} ${customDate.getHours()} ${customDate.getDate()} ${
    customDate.getMonth() + 1
  } ${customDate.getDay()}`;
  console.log({ dateGiven: date, customDate, customCronSyntax: cronSyntax });
  return cronSyntax;
};

module.exports = {
  schedulerBackup,
  customCronsyntax,
};
