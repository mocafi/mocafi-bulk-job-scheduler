const Joi = require('joi');
const cron = require('node-cron');
const { customCronsyntax } = require('../helpers');
const CronExpression = require('./cron-expressions');

const createTask = Joi.object().keys({
  query: {
    name: Joi.string().required(),
    process: Joi.string().required(),
    cronExpression: Joi.string()
      .custom((value, helper) =>
        CronExpression[value] || value === 'custom'
          ? value
          : helper.error('any.invalid')
      )
      .required(),
    description: Joi.string().allow('').optional(),
    prodId: Joi.string().required(),
    programName: Joi.string().required(),
    timesToBeProcessed: Joi.number().min(1).optional(),
    customDate: Joi.date()
      .iso()
      .custom((value, helper) =>
        cron.validate(customCronsyntax(value))
          ? value
          : helper.error('any.invalid')
      )
      .when('cronExpression', {
        is: Joi.string().valid('custom'),
        then: Joi.required(),
        otherwise: Joi.forbidden(),
      }),
  },
});

const isMongoId = (message) =>
  Joi.string().pattern(new RegExp('^[0-9a-fA-F]{24}$')).required().messages({
    'string.pattern.base': message,
  });

const createIrcTask = Joi.object().keys({
  body: {
    dbName: Joi.string().required(),
    taskId: isMongoId('invalid task id'),
  },
});

const deleteIrcTask = Joi.object().keys({
  params: {
    taskId: isMongoId('invalid task id'),
  },
});

module.exports = {
  createTask,
  createIrcTask,
  deleteIrcTask,
};
