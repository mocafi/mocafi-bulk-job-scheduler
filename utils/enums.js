const jobStatusEnum = {
  created: 'created',
  pendingForReview: 'pending for review',
  active: 'active',
  canceled: 'canceled',
  completed: 'completed',
  paused: 'paused',
  error: 'error',
};

const jobTypeEnum = {
  recurrent: 'recurrent',
  unique: 'unique',
};

const jobProcessEnum = {
  loadFunds: 'load-funds',
};

const uploadFileTypes = {
  CSV: 'text/csv',
};

module.exports = {
  jobStatusEnum,
  jobProcessEnum,
  jobTypeEnum,
  uploadFileTypes,
};
