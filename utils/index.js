const { awsS3 } = require('../services/aws-s3');
const { parse } = require('csv-parse');

const parsedCSV = ({ Bucket, Key, columns = [] }) =>
  new Promise((resolve, reject) => {
    awsS3.getObject(
      {
        Bucket,
        Key,
      },
      (error, data) => {
        if (error) return reject(error);
        parse(
          data.Body.toString('utf-8'),
          { fromLine: 2, columns },
          (err, cards) => {
            if (err) return reject(err);
            resolve(cards);
          }
        );
      }
    );
  });

module.exports = { parsedCSV };
