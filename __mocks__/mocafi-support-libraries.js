const msl = jest.createMockFromModule('mocafi-support-libraries');

msl.Mocafi = {
  getToken: jest.fn(),
  postServiceV2: jest.fn().mockImplementation(() => {
    return {
      Error: 1,
    };
  }),
};

msl.Slack = { sendSlackMessage: jest.fn() };

msl.AWSLogger = jest.fn().mockImplementation(() => ({
  info: jest.fn(),
  error: jest.fn(),
}));

module.exports = msl;
