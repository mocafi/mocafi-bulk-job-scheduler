const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const sqs = require('../services/sqs');
const {
  updateOneIrcCard,
  updateManyIrcCard,
  updateFundingRequest,
} = require('../services/irc');
const { parsedCSV } = require('../utils');

const loadFunds = async (task, dbName) => {
  await updateManyIrcCard(
    { orderId: task.orderId },
    {
      loadFundsQueue: true,
    },
    { dbName }
  );

  sqs.writeSQS(
    JSON.stringify({
      orderId: `${task.orderId}`,
      dbName,
      process: 'irc-portal',
      type: 'order-load-funds',
      loadType: task.loadType,
      amount: task.loadAmount,
      description: task.description,
    })
  );
};

const loadFundsFromCSV = async (task, dbName) => {
  const Bucket = task.sourceLocation.split('/')[2].split('.')[0];
  const payments = await parsedCSV({
    Key: task.fileName,
    Bucket,
    columns: ['PRN', 'amount'],
  });

  for (const payment of payments) {
    await updateOneIrcCard(
      { orderId: task.orderId, PRN: payment.PRN },
      { loadFundsQueue: true, amountToFund: parseFloat(payment.amount) },
      { dbName }
    );
  }

  sqs.writeSQS(
    JSON.stringify({
      orderId: `${task.orderId}`,
      dbName,
      process: 'irc-portal',
      type: 'order-load-funds',
      loadType: task.loadType || 'RL',
      description: task.description,
      partial: true,
    })
  );
};

const processFundingRequest = async (fundingRequestId, dbName) => {
  try {
    await updateFundingRequest(
      { _id: fundingRequestId },
      {
        status: 'processing',
      },
      { dbName }
    );

    sqs.writeSQS(
      JSON.stringify({
        dbName,
        process: 'irc-portal',
        type: 'process-funding-requests',
        approvedRequests: [fundingRequestId],
        approvedBy: 'automatic process',
      })
    );
  } catch (error) {
    logger.error(
      `Error processing funding request ${fundingRequestId} ${dbName} - ${error.message}`
    );
  }
};

module.exports = {
  loadFundsFromCSV,
  loadFunds,
  processFundingRequest,
};
