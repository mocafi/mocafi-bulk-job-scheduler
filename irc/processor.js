const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const { jobProcessEnum } = require('../utils/enums');
const { updateIrcTask } = require('../services/irc');
const handlers = require('./handlers');

const processTask = (task, dbName) => {
  switch (task.process) {
    case jobProcessEnum.loadFunds:
      processLoadFunds(task, dbName);
      break;

    default:
      break;
  }
};

const processLoadFunds = async (task, dbName) => {
  try {
    const handler =
      task.fileName && task.sourceLocation
        ? handlers.loadFundsFromCSV
        : handlers.loadFunds;

    await handler(task, dbName);
  } catch (error) {
    logger.error(`processLoadFunds() Error - ${error.message}`);
  } finally {
    updateIrcTask(
      { _id: task._id },
      {},
      {
        dbName,
        customOperators: {
          $push: {
            processHistory: new Date(),
          },
        },
      }
    );
  }
};

module.exports = {
  processTask,
};
