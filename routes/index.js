const express = require('express');
const oauthServer = require('mocafi-support-libraries').OAuth;
const {
  createTaskController,
  deleteTaskController,
  getCronExpressionsController,
  getTaskController,
  getTaskByIdController,
  editTaskController,
} = require('../controllers');
const { csvTaskUploaderToS3 } = require('../middlewares');
const { schemaValidator } = require('../middlewares/schemaValidator');
const schemas = require('../utils/schemas');

const schedulerRouter = express.Router();

schedulerRouter.use(oauthServer.middleware);

schedulerRouter.get('/cron-expressions', getCronExpressionsController);

schedulerRouter.get('/:id', getTaskByIdController);

schedulerRouter.get('/', getTaskController);

schedulerRouter.post(
  '/',
  [schemaValidator(schemas.createTask), csvTaskUploaderToS3],
  createTaskController
);

schedulerRouter.put('/:id', editTaskController);

schedulerRouter.delete('/:id', deleteTaskController);

module.exports = schedulerRouter;
