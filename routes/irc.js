const express = require('express');

const { schemaValidator } = require('../middlewares/schemaValidator');
const ircController = require('../controllers/irc');
const schemas = require('../utils/schemas');
const { TokenGenerator } = require('mocafi-crypto-library');
const { jwtValidatorMw } = require('mocafi-express-middleware-library');

const authTokenOptions = {
  expiresIn: process.env.JWT_AUTH_TOKEN_EXPIRES_IN || '10m',
  algorithm: process.env.JWT_ALGORITHM || 'HS512',
};
const authTokenGenerator = new TokenGenerator(
  process.env.JWT_AUTH_TOKEN_SECRET_KEY,
  authTokenOptions
);
const authTokenMiddleware = jwtValidatorMw(authTokenGenerator, 'authToken');

const ircRouter = express.Router();
ircRouter.use(authTokenMiddleware);

ircRouter.post(
  '/',
  [schemaValidator(schemas.createIrcTask)],
  ircController.createIrcTask
);

ircRouter.delete(
  '/:taskId',
  [schemaValidator(schemas.deleteIrcTask)],
  ircController.deleteIrcTask
);

module.exports = ircRouter;
