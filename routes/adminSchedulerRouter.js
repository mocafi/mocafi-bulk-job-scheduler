const express = require('express');
const {
  createTaskController,
  deleteTaskController,
  getCronExpressionsController,
  getTaskControllerOneapp,
  getTaskByIdController,
  editTaskController,
} = require('../controllers');
const { csvTaskUploaderToS3, checkUserPermissions } = require('../middlewares');
const { schemaValidator } = require('../middlewares/schemaValidator');
const schemas = require('../utils/schemas');
const { cognitoJwtValidatorMw } = require('mocafi-express-middleware-library');

const adminSchedulerRouter = express.Router();

const cognitoTokenMiddleware = cognitoJwtValidatorMw(
  process.env.ADMIN_PORTAL_USERPOOL_ID,
  'user',
  null,
  true
);

adminSchedulerRouter.use(cognitoTokenMiddleware);

adminSchedulerRouter.get(
  '/cron-expressions',
  checkUserPermissions(['admin-write', 'admin-read', 'all-write', 'all-read']),
  getCronExpressionsController
);

adminSchedulerRouter.get(
  '/:id',
  checkUserPermissions(['admin-write', 'admin-read', 'all-write', 'all-read']),
  getTaskByIdController
);

adminSchedulerRouter.get(
  '/',
  checkUserPermissions(['admin-write', 'admin-read', 'all-write', 'all-read']),
  getTaskControllerOneapp
);

adminSchedulerRouter.post(
  '/',
  checkUserPermissions(['admin-write', 'all-write']),
  [schemaValidator(schemas.createTask), csvTaskUploaderToS3],
  createTaskController
);

adminSchedulerRouter.put(
  '/:id',
  checkUserPermissions(['admin-write', 'all-write']),
  editTaskController
);

adminSchedulerRouter.delete(
  '/:id',
  checkUserPermissions(['admin-write', 'all-write']),
  deleteTaskController
);

module.exports = adminSchedulerRouter;
