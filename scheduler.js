const cron = require('node-cron');
const { Slack: slack, Mocafi: mocafi } = require('mocafi-support-libraries');
const logger = require('mocafi-support-libraries').AWSLogger('scheduler');
const queryString = require('query-string');
const { getTaskById, updateTaskById } = require('./services');
const { writeSQS } = require('./services/sqs');
const {
  getOrdersToProcess,
  getIrcTaskById,
  updateIrcTask,
  getTasksToPurge,
  getFundingRequestsToProcess,
} = require('./services/irc');
const CronExpression = require('./utils/cron-expressions');
const moment = require('moment');
const { jobStatusEnum, jobTypeEnum } = require('./utils/enums');
const irc = require('./irc/processor');
const { processFundingRequest } = require('./irc/handlers');
const { getPassManualKycReviews } = require('./services/oneapp');

let scheduler = {};

const channel =
  process.env.NODE_ENV === 'production' ? '#admin-logs' : '#dev-logs';

// HARDCODED TASK FOR DAILY REPORTING AT 3AM AMERICA/NEW_YORK
const dailyReportTask = () => {
  cron.schedule(
    '0 03 * * *',
    async () => {
      logger.info('sending SQS event to daily reporting');

      // -1 day in ms
      const fromDate = new Date(Date.now() - 86400000).toISOString();

      const toDate = new Date().toISOString();

      // sqs for load funds daily report
      writeSQS(
        JSON.stringify({
          fromDate,
          toDate,
          process: 'summary-reports',
          bulkProcess: 'load-funds',
        })
      );

      // sqs for create adjustments daily report
      writeSQS(
        JSON.stringify({
          fromDate,
          toDate,
          process: 'summary-reports',
          bulkProcess: 'create-adjustment',
        })
      );
    },
    { timezone: 'America/New_York' }
  );
};

const cronSyntaxOrders =
  process.env.NODE_ENV === 'production'
    ? CronExpression.EVERY_DAY_AT_10AM_1PM_5PM_8PM
    : CronExpression.EVERY_MINUTE;

// HARDCODED TASK TO PROCESS ORDERS FROM IRC PORTAL
const processOrdersIrcTask = () => {
  cron.schedule(
    cronSyntaxOrders,
    async () => {
      try {
        if (process.env.NODE_ENV === 'production') {
          logger.info('Task for processing orders from portal irc triggered');
        }

        const orders = await getOrdersToProcess();

        orders.forEach((order) => {
          logger.info(
            `Sending SQS for ORDER ${order._id.toString()} ${order.prodId}`
          );
          writeSQS(
            JSON.stringify({
              orderId: order._id.toString(),
              dbName: order.prodId,
              process: 'irc-portal',
              type: 'order-approved',
            })
          );
        });
      } catch (error) {
        logger.error(error.message);
      }
    },
    {
      timezone: 'America/New_York',
    }
  );
};

const newTask = (data, { timezone, isIRC = false, dbName }) => {
  scheduler[data._id] = cron.schedule(
    data.cronSyntax,
    () => {
      isIRC ? taskTriggerIRC(data._id, dbName) : taskTrigger(data._id);
    },
    { timezone }
  );
};

const destroyTask = (id) => {
  if (scheduler[id]) {
    scheduler[id].stop();
    scheduler[id] = null;
    return true;
  }
  return false;
};

const destroyAllTasks = () => {
  const tasksIds = Object.keys(scheduler);
  tasksIds.forEach((taskId) => {
    destroyTask(taskId);
  });
};

const taskTrigger = async (taskId) => {
  try {
    const task = await getTaskById(taskId);

    const {
      _id: task_Id,
      prodId,
      programName,
      process,
      scheduledBy,
      templateId,
      sourceLocation,
      timesToBeProcessed = undefined,
      timesProcessed,
      fileName,
    } = task;

    const name = `count_${timesProcessed.length + 1}_${fileName}`;

    const queryStringified = queryString.stringify(
      {
        prodId,
        programName,
        name,
        templateId,
        sourceLocation,
        taskId: task_Id,
        originalName: fileName,
      },
      {
        skipEmptyString: true,
        arrayFormat: 'comma',
      }
    );

    const response = await mocafi.postServiceV2(
      `galileo/admin/bulk/scheduler/${process}?${queryStringified}`,
      {},
      { 'mocafi-username': scheduledBy }
    );

    let status = '';

    if (response.Error !== 0) {
      status = 'error';
      slack.sendSlackMessage({
        username: `TASKS SCHEDULER - FAIL`,
        icon_emoji: ':x:',
        text: `ERROR in task '${task.name}' scheduled by ${task.scheduledBy}`,
        channel,
      });
    } else {
      status = 'success';
      slack.sendSlackMessage({
        username: `TASKS SCHEDULER`,
        icon_emoji: ':timer_clock:',
        text: `'${task.name}' has been triggered successfully. Scheduled by ${task.scheduledBy}`,
        channel,
      });
      logger.info(
        `task ${task.name} has been triggered successfully. Scheduled by ${task.scheduledBy}`
      );
    }

    const responseCheck = status === 'error' ? { ...response.Error } : response;

    if (
      timesToBeProcessed !== undefined &&
      timesToBeProcessed == timesProcessed.length + 1
    ) {
      destroyTask(task_Id);

      updateTaskById(
        task_Id,
        { status: 'completed' },
        {
          $push: {
            timesProcessed: {
              date: new Date(),
              status,
              response: responseCheck,
            },
          },
        }
      );

      logger.info(`${task.name} has reached its times`);
    } else {
      updateTaskById(
        task_Id,
        {},
        {
          $push: {
            timesProcessed: {
              date: new Date(),
              status,
              response: responseCheck,
            },
          },
        }
      );
    }
  } catch (error) {
    console.log(error);
    logger.error(`Error triggering task Id=${taskId}`);
  }
};

const completeIrcTask = async (taskId, dbName) => {
  try {
    logger.info(`completeIrcTask() - task ${taskId} ${dbName} completed`);
    await updateIrcTask(
      { _id: taskId },
      { status: jobStatusEnum.completed },
      {
        dbName,
        customOperators: {
          $push: {
            history: {
              date: new Date(),
              action: jobStatusEnum.completed,
            },
          },
        },
      }
    );
    destroyTask(taskId);
  } catch (error) {
    logger.error(
      `completeIrcTask() error - ${error.message}. task ${taskId} ${dbName}`
    );
  }
};

// HARDCODED TASK TO COMPLETE TASKS WHEN END DATE IS PAST
const purgeCompletedIrcTasks = async () => {
  cron.schedule(CronExpression.EVERY_MINUTE, async () => {
    try {
      const tasksToPurge = await getTasksToPurge();

      for (const { _id, prodId } of tasksToPurge) {
        await completeIrcTask(_id, prodId);
      }
    } catch (error) {
      logger.error(`purgeCompletedIrcTasks() error - ${error.message}`);
    }
  });
};

const taskTriggerIRC = async (taskId, dbName) => {
  try {
    const task = await getIrcTaskById(taskId, { dbName });

    logger.info(`task ${task._id} ${dbName} has been triggered`);

    if (!task) {
      logger.error(`taskTriggerIRC() error - task ${taskId} not found`);
      destroyTask(taskId);
      return;
    }

    if (task.status !== jobStatusEnum.active) {
      logger.error(
        `taskTriggerIRC() error - task ${taskId} status is not active - current status ${task.status}`
      );
      destroyTask(taskId);
      return;
    }

    if (moment().isAfter(task.endsAt)) {
      await completeIrcTask(task._id, dbName);
      logger.info(
        `taskTriggerIRC() - task ${taskId} ${dbName} end date is past`
      );
      return;
    }

    irc.processTask(task, dbName);

    if (task.type === jobTypeEnum.unique) {
      await completeIrcTask(task._id, dbName);
      logger.info(`taskTriggerIRC() - task ${taskId} ${dbName} completed`);
    }
  } catch (error) {
    logger.error(
      `taskTriggerIRC() error - ${error.message} while executing task ${taskId} for db ${dbName}`
    );
  }
};

const automaticFundingRequestApproval = async () => {
  cron.schedule(CronExpression.EVERY_3_MINUTES, async () => {
    try {
      const fundingRequests = await getFundingRequestsToProcess();

      for (const { _id, prodId } of fundingRequests) {
        if (prodId === '2950') {
          await processFundingRequest(_id, prodId);
        }
      }
    } catch (error) {
      logger.error(
        `automaticFundingRequestApproval() error - ${error.message}`
      );
    }
  });
};

const automaticManualReview = async () => {
  cron.schedule(CronExpression.EVERY_10_MINUTES, async () => {
    try {
      const passedManualKycReviews = await getPassManualKycReviews();

      for (const { _id } of passedManualKycReviews) {
        const id = `${_id}`;
        writeSQS(
          JSON.stringify({
            manualKycReviewId: _id,
            process: 'oneapp',
            type: 'process-manual-kyc-review',
          }),
          {
            MessageDeduplicationId: id,
            MessageGroupId: id,
          }
        );
      }
    } catch (error) {
      logger.error(`automaticManualReview() error - ${error.message}`);
    }
  });
};

const runTasks = () => {
  dailyReportTask();
  processOrdersIrcTask();
  purgeCompletedIrcTasks();
  automaticFundingRequestApproval();
  automaticManualReview();
};

module.exports = {
  newTask,
  destroyTask,
  runTasks,
  destroyAllTasks,
  taskTriggerIRC,
  taskTrigger,
  completeIrcTask,
};
