FROM node:20.18-bookworm as builder

WORKDIR /usr/src/app
COPY . /usr/src/app

# Build ARGs coming from docker build --build-arg
ARG AWS_ACCOUNT_ID_DOCKER
ARG AWS_ACCESS_KEY_ID_DOCKER
ARG AWS_SECRET_ACCESS_KEY_DOCKER
ARG AWS_DEFAULT_REGION=us-east-1

RUN curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
   && unzip awscliv2.zip \
   && aws/install \
   && rm -rf \
   awscliv2.zip \
   aws \
   /usr/local/aws-cli/v2/*/dist/aws_completer \
   /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
   /usr/local/aws-cli/v2/*/dist/awscli/examples 

RUN aws --version
RUN aws configure set aws_access_key_id "$AWS_ACCESS_KEY_ID_DOCKER"
RUN aws configure set aws_secret_access_key "$AWS_SECRET_ACCESS_KEY_DOCKER"
RUN aws configure set region "$AWS_DEFAULT_REGION"  
RUN aws codeartifact login --tool npm --domain mocafi-support-libraries --domain-owner "$AWS_ACCOUNT_ID_DOCKER" --repository mocafi-support-library
RUN echo "export CODEARTIFACT_AUTH_TOKEN='aws codeartifact get-authorization-token --domain mocafi-support-libraries --domain-owner "$AWS_ACCOUNT_ID_DOCKER" --query authorizationToken --output text'" 
       
RUN npm config set engine-strict=true
#add when this has proper unit tests RUN npm install-ci-test
RUN NODE_ENV=production npm ci

FROM node:20.18-bookworm-slim 

# Add tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /home/node/tini
RUN chmod +x /home/node/tini

USER node
RUN mkdir -p /home/node/app
WORKDIR /home/node/app
COPY --chown=node:node --from=builder /usr/src/app .

ENTRYPOINT ["/home/node/tini", "--"]

#doesn't matter for now but fix later ENV NODE_ENV=production
ENV PORT=6021

EXPOSE 6021

CMD ["node", "server.js"]
